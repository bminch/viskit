from setuptools import setup

setup(
    name="pyviskit",
    version="0.0.1",
    packages=["pyviskit"],
    install_requires=["pyviskit_bindings", "pandas"],
    zip_safe=False,
)
