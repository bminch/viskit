ITERATIONS = "iterations"
NEAREST_NEIGHBORS_COUNT = "nearest_neighbors_count"
RANDOM_NEIGHBORS_COUNT = "random_neighbors_count"
BINARY_DISTANCES = "binary_distances"
REVERSE_NEIGHBORS_STEPS = "reverse_neighbors_steps"
REVERSE_NEIGHBORS_COUNT = "reverse_neighbors_count"
L1_STEPS = "l1_steps"

DEFAULTS = {
    "iterations": 2500,
    "nearest_neighbors_count": 2,
    "random_neighbors_count": 1,
    "binary_distances": True,
    "reverse_neighbors_steps": 0,
    "reverse_neighbors_count": 0,
    "l1_steps": 0,
}
